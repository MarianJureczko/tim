package org.tim.databaseSeed;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.tim.entities.Message;
import org.tim.entities.Project;
import org.tim.repositories.MessageRepository;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class MessageSeeder {

    private final MessageRepository messageRepository;

    public Map<String, Message> initMessages(Map<String, Project> projects) {

        Map<String, Message> messages = new HashMap<>();

        Message messageM1 = new Message("welcome1", "Groceries you’ll love, perfectly delivered.", projects.get("projectP1"));
        messageM1.setDescription("The first page of the website for selling food and chemical products.");
        messages.put("messageM1", messageRepository.save(messageM1));

        Message messageM2 = new Message("welcome2", "Low Price Promise.", projects.get("projectP1"));
        messageM2.setDescription("The first page of the website for selling food and chemical products.");
        messages.put("messageM2", messageRepository.save(messageM2));

        Message messageM3 = new Message("business1", "Opracowaliśmy i stosujemy unikalny model biznesowy, który mocno nas pozycjonuje, " +
                "ponieważ coraz więcej konsumentów decyduje się na zakupy w Internecie.", projects.get("projectP2"));
        messageM3.setDescription("Pierwsza strona prezentująca przedsiębiorstwo.");
        messages.put("messageM3", messageRepository.save(messageM3));

        Message messageM4 = new Message("business2", "Naszym strategicznym celem jest dostosowanie interesów naszych klientów," +
                " inwestorów i innych interesariuszy do zapewnienia długoterminowej wartości dla akcjonariuszy.", projects.get("projectP2"));
        messageM4.setDescription("Pierwsza strona prezentująca przedsiębiorstwo.");
        messages.put("messageM4", messageRepository.save(messageM4));

        return messages;
    }
}
